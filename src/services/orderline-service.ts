import type { OrderLine, Product } from "@/entities";
import axios from "axios";

const BASE_URL = 'http://localhost:8080/api';

export async function getAllOrders() {
    const response = await axios.get<OrderLine[]>(BASE_URL + '/orderlines');
    return response.data;
}

export async function getOrdersById(id: number) {
    const response = await axios.get<OrderLine>(BASE_URL + '/orderlines/${id}');
    return response.data;
}

export async function updateOrders (orderLine: OrderLine) {
    const response = await axios.put<OrderLine>(BASE_URL + '/orderlines/${id}', orderLine);
    return response.data;
}

export async function updateProductQuantityInCart(id: any, quantity: number, cart: OrderLine[]) {
    const orderLine = cart.find(item => item.product.id === id);
    if (orderLine) {
        orderLine.quantity = quantity;
    }
}
export async function deleteProductFromCart(id: any, cart: Product[]) {
    const index = cart.findIndex(item => item.id === id);
    if (index !== -1) {
        cart.splice(index, 1);
    }
}