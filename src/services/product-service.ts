import type { OrderLine, Product } from '@/entities';
import axios from 'axios';

const BASE_URL = 'http://localhost:8080/api';

const axiosInstance = axios.create({
  baseURL: BASE_URL,
});

export async function getAllProducts() {
  const response = await axios.get<Product[]>(BASE_URL + '/products');
  return response.data;
}

export async function getProductById(id: number) {
  const response = await axios.get<Product>(BASE_URL + `/products/${id}`);
  return response.data;
}

export async function updateProductQuantityInCart(id: number, quantity: number, cart: OrderLine[]) {
  const orderLine = cart.find(item => item.product.id === id);
  if (orderLine) {
    orderLine.quantity = quantity;
  }
}

export async function deleteProductFromCart(id: number, cart: Product[]) {
  const index = cart.findIndex(item => item.id === id);
  if (index !== -1) {
    cart.splice(index, 1);
  }
}