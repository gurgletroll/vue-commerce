export interface Product {
heartColor: any;
    id: number;
    name: string;
    price: number;
    photo: string;
    description: string;
    stock: boolean;
    quantity: number;
    isFavorite: boolean;
}

export interface OrderLine{
    id?:number;
    product: Product;
    quantity: number;
}

export interface User {
    id?:number;
    email:string;
    password?:string;
    role?:string;
}

export interface Review {
    id?:number;
    product: Product;
    user: User;
    comment: string;
    rating: number;
    date: Date;
    
}