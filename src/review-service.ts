import axios from "axios";
import type { Review } from "./entities";


export async function fetchTestOne(id:any){
    const response = await axios.get<Review>('http://localhost:8080/api/reviews'+id);
    return response.data;
}
export async function fetchTestAll(){
    const response = await axios.get<Review[]>('http://localhost:8080/api/reviews');
    return response.data;
}

export async function postReview(review:Review,idProduct:any) {
    const response = await axios.post<Review> ('http://localhost:8080/api/reviews/product/'+idProduct, review);
    return response.data
}


