import { defineStore } from "pinia";
import type { Product, OrderLine } from "@/entities";
import { ref, computed } from "vue";

export const useCartStore = defineStore('cart', () => {
  const cartItems = ref<Product[]>([]);

  const storedCart = localStorage.getItem('cart');
  if (storedCart) {
    cartItems.value = JSON.parse(storedCart);
  }

  function addToCart(product: Product) {
    const existingIndex = cartItems.value.findIndex(item => item.id === product.id);
    if (existingIndex !== -1) {
      cartItems.value[existingIndex].quantity++;
    } else {
      product.quantity = 1;
      cartItems.value.push(product);
    }
    localStorage.setItem('cart', JSON.stringify(cartItems.value));
  }


  // a revoir pour supprimer l'element quelque soit la quantité deja mise dans le panier:
  function removeItemCompletely(product: Product) {
    const existingIndex = cartItems.value.findIndex(item => item.id === product.id);
    if (existingIndex !== -1) {
        cartItems.value.splice(existingIndex, 1);
    }
    localStorage.setItem('cart', JSON.stringify(cartItems.value));
  }

  function removeFromCart(product: Product) {
    const existingIndex = cartItems.value.findIndex(item => item.id === product.id);
    if (existingIndex !== -1) {
      if (cartItems.value[existingIndex].quantity > 1) {
        cartItems.value[existingIndex].quantity--;
      } else {
        cartItems.value.splice(existingIndex, 1);
      }
    }
    localStorage.setItem('cart', JSON.stringify(cartItems.value));
  }
  

  function clearCart() {
    cartItems.value = [];
    localStorage.removeItem('cart');
  }

  const totalPrice = computed(() => {
    return cartItems.value.reduce((total, product) => total + product.quantity * product.price, 0);
  });

  return { cartItems, addToCart, removeFromCart, removeItemCompletely, clearCart, totalPrice };
});