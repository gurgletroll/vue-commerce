import type { User } from "@/entities";
import { fetchLogin, fetchLogout } from "@/services/auth-service";
import { defineStore } from "pinia";
import { ref } from "vue";


export const useAuth = defineStore('auth', () => {

    const user = ref<User>();
    const stored = localStorage.getItem('user');
    if(stored) {
        user.value = JSON.parse(stored);
    }

    async function login(email:string,password:string) {
        const data = await fetchLogin(email,password);
        localStorage.setItem('user', JSON.stringify(data));
        user.value = data;
    }
    async function logout() {
        await fetchLogout();
        localStorage.removeItem('user');
        user.value = undefined;
    }
    
    return {user,login,logout};
})


//Faire un nouveau store pour le panier qui va contenir une ref avec un tableau d'orderline et le truc du localstorage comme ici
//Et une fonction addToCart(product:Product) et qui va le push dans la ref d'orderline avec une quantity en plus
//et si on veut faire les choses bien, faire que si on ajoute un produit qui est déjà dans le panier, on augmente juste la quantité de 1

