import { createRouter, createWebHistory } from 'vue-router';
import Cart from '@/components/Cart.vue';
import LoginView from '@/components/LoginView.vue';
import ProductDetails from '@/components/ProductDetails.vue';
import ProductList from '@/components/ProductList.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'productList',
      component: ProductList,


    },
    {
      path: '/cart',
      name: 'cart',
      component: Cart,
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView,
    },
    {
      path: '/products/:id',
      name: 'productDetails',
      component: ProductDetails,
      props: true,
    },
  ],
});

export default router;